# Python/Riak TPC-C #

Start by defining the configuration file that gets returned with by the 
'makeDefaultConfig' function in your driver and  then implement the data 
loading part first, since that will guide how you actually execute the 
transactions. 

$ python ./tpcc.py --print-config riak > riak.config

Make any changes you need to 'riak.config' (e.g., passwords, hostnames). 
Then test the loader:

$ python ./tpcc.py --no-execute --config=riak.config riak

## Credits ##

Based on

https://github.com/apavlo/py-tpcc/
