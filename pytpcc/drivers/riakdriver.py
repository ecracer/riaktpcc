# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------
# Copyright (C) 2015
# Lucas Brutschy
# https://people.inf.ethz.ch/blucas/
#
# Original Python Version:
# Copyright (C) 2011
# Andy Pavlo
# http://www.cs.brown.edu/~pavlo/
#
# Original Java Version:
# Copyright (C) 2008
# Evan Jones
# Massachusetts Institute of Technology
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, 
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# -----------------------------------------------------------------------

from __future__ import print_function
from __future__ import print_function
from __future__ import with_statement

import httplib
import json
import logging
import sys
import time
import urllib2
from pprint import pformat

from kazoo.client import KazooClient
from riak import RiakClient, RiakError

from abstractdriver import *
from drivers.riakupdate import RiakMapUpdate
from util.retcounter import RetCounter

# -----------------------------------------------------------------------
# Helper fields
# -----------------------------------------------------------------------
S_QUANTITY_COUNTER = "S_QUANTITY_COUNTER"

# -----------------------------------------------------------------------
# Denormalized fields
# -----------------------------------------------------------------------
ORDER_IS_NEW = "ORDER_IS_NEW"
ORDER_LINE_SET = "ORDER_LINE_SET"

# -----------------------------------------------------------------------
# Denormalized tables
# -----------------------------------------------------------------------
DENORMALIZED_TABLE = {
    constants.TABLENAME_NEW_ORDER: {
        "NO_O_ID": "O_ID",
        "NO_D_ID": "O_D_ID",
        "NO_W_ID": "O_W_ID",
    },
    constants.TABLENAME_ORDER_LINE: {
        "OL_O_ID": "O_ID",
        "OL_D_ID": "O_D_ID",
        "OL_W_ID": "O_W_ID"
    }
}

# -----------------------------------------------------------------------
# Table columns.
# -----------------------------------------------------------------------
TABLE_COLUMNS = {
    constants.TABLENAME_ITEM: [
        "I_ID",  # INTEGER
        "I_IM_ID",  # INTEGER
        "I_NAME",  # VARCHAR
        "I_PRICE",  # FLOAT
        "I_DATA",  # VARCHAR
    ],
    constants.TABLENAME_WAREHOUSE: [
        "W_ID",  # SMALLINT
        "W_NAME",  # VARCHAR
        "W_STREET_1",  # VARCHAR
        "W_STREET_2",  # VARCHAR
        "W_CITY",  # VARCHAR
        "W_STATE",  # VARCHAR
        "W_ZIP",  # VARCHAR
        "W_TAX",  # FLOAT
        "W_YTD",  # FLOAT
    ],
    constants.TABLENAME_DISTRICT: [
        "D_ID",  # TINYINT
        "D_W_ID",  # SMALLINT
        "D_NAME",  # VARCHAR
        "D_STREET_1",  # VARCHAR
        "D_STREET_2",  # VARCHAR
        "D_CITY",  # VARCHAR
        "D_STATE",  # VARCHAR
        "D_ZIP",  # VARCHAR
        "D_TAX",  # FLOAT
        "D_YTD",  # FLOAT
        "D_NEXT_O_ID",  # INT
    ],
    constants.TABLENAME_CUSTOMER: [
        "C_ID",  # INTEGER
        "C_D_ID",  # TINYINT
        "C_W_ID",  # SMALLINT
        "C_FIRST",  # VARCHAR
        "C_MIDDLE",  # VARCHAR
        "C_LAST",  # VARCHAR
        "C_STREET_1",  # VARCHAR
        "C_STREET_2",  # VARCHAR
        "C_CITY",  # VARCHAR
        "C_STATE",  # VARCHAR
        "C_ZIP",  # VARCHAR
        "C_PHONE",  # VARCHAR
        "C_SINCE",  # TIMESTAMP
        "C_CREDIT",  # VARCHAR
        "C_CREDIT_LIM",  # FLOAT
        "C_DISCOUNT",  # FLOAT
        "C_BALANCE",  # FLOAT
        "C_YTD_PAYMENT",  # FLOAT
        "C_PAYMENT_CNT",  # INTEGER
        "C_DELIVERY_CNT",  # INTEGER
        "C_DATA",  # VARCHAR
    ],
    constants.TABLENAME_STOCK: [
        "S_I_ID",  # INTEGER
        "S_W_ID",  # SMALLINT
        "S_QUANTITY",  # INTEGER
        "S_DIST_01",  # VARCHAR
        "S_DIST_02",  # VARCHAR
        "S_DIST_03",  # VARCHAR
        "S_DIST_04",  # VARCHAR
        "S_DIST_05",  # VARCHAR
        "S_DIST_06",  # VARCHAR
        "S_DIST_07",  # VARCHAR
        "S_DIST_08",  # VARCHAR
        "S_DIST_09",  # VARCHAR
        "S_DIST_10",  # VARCHAR
        "S_YTD",  # INTEGER
        "S_ORDER_CNT",  # INTEGER
        "S_REMOTE_CNT",  # INTEGER
        "S_DATA",  # VARCHAR
    ],
    constants.TABLENAME_ORDERS: [
        "O_ID",  # INTEGER
        "O_C_ID",  # INTEGER
        "O_D_ID",  # TINYINT
        "O_W_ID",  # SMALLINT
        "O_ENTRY_D",  # TIMESTAMP
        "O_CARRIER_ID",  # INTEGER
        "O_OL_CNT",  # INTEGER
        "O_ALL_LOCAL",  # INTEGER
    ],
    constants.TABLENAME_NEW_ORDER: [
        "NO_O_ID",  # INTEGER
        "NO_D_ID",  # TINYINT
        "NO_W_ID",  # SMALLINT
    ],
    constants.TABLENAME_ORDER_LINE: [
        "OL_O_ID",  # INTEGER
        "OL_D_ID",  # TINYINT
        "OL_W_ID",  # SMALLINT
        "OL_NUMBER",  # INTEGER
        "OL_I_ID",  # INTEGER
        "OL_SUPPLY_W_ID",  # SMALLINT
        "OL_DELIVERY_D",  # TIMESTAMP
        "OL_QUANTITY",  # INTEGER
        "OL_AMOUNT",  # FLOAT
        "OL_DIST_INFO",  # VARCHAR
    ],
    constants.TABLENAME_HISTORY: [
        "H_C_ID",  # INTEGER
        "H_C_D_ID",  # TINYINT
        "H_C_W_ID",  # SMALLINT
        "H_D_ID",  # TINYINT
        "H_W_ID",  # SMALLINT
        "H_DATE",  # TIMESTAMP
        "H_DATA",  # VARCHAR
    ],
}
TABLE_INDEXES = {
    constants.TABLENAME_ITEM: [
        "I_ID",
    ],
    constants.TABLENAME_WAREHOUSE: [
        "W_ID",
    ],
    constants.TABLENAME_DISTRICT: [
        "D_ID",
        "D_W_ID",
    ],
    constants.TABLENAME_CUSTOMER: [
        "C_ID",
        "C_D_ID",
        "C_W_ID",
    ],
    constants.TABLENAME_STOCK: [
        "S_I_ID",
        "S_W_ID",
    ],
    constants.TABLENAME_ORDERS: [
        "O_ID",
        "O_D_ID",
        "O_W_ID",
        "O_C_ID",
    ],
    constants.TABLENAME_NEW_ORDER: [
        "NO_O_ID",
        "NO_D_ID",
        "NO_W_ID",
    ],
    constants.TABLENAME_ORDER_LINE: [
        "OL_O_ID",
        "OL_D_ID",
        "OL_W_ID",
        "OL_NUMBER"
    ],
    constants.TABLENAME_HISTORY: []
}

TABLE_COUNTERS = {
    constants.TABLENAME_ITEM: [],
    constants.TABLENAME_WAREHOUSE: [
        "W_YTD"
    ],
    constants.TABLENAME_DISTRICT: [
        "D_YTD",
        "D_NEXT_O_ID",  # INT
    ],
    constants.TABLENAME_CUSTOMER: [
        "C_BALANCE",  # FLOAT
        "C_YTD_PAYMENT",  # FLOAT
        "C_PAYMENT_CNT",  # INTEGER
    ],
    constants.TABLENAME_STOCK: [
        S_QUANTITY_COUNTER,
        "S_QUANTITY",
        "S_ORDER_CNT",
        "S_YTD"
    ],
    constants.TABLENAME_ORDERS: [],
    constants.TABLENAME_NEW_ORDER: [],
    constants.TABLENAME_ORDER_LINE: [],
    constants.TABLENAME_HISTORY: [],
}
TABLE_SETS = {
    constants.TABLENAME_ITEM: [],
    constants.TABLENAME_WAREHOUSE: [],
    constants.TABLENAME_DISTRICT: [],
    constants.TABLENAME_CUSTOMER: [],
    constants.TABLENAME_STOCK: [],
    constants.TABLENAME_ORDERS: [
        ORDER_LINE_SET,
    ],
    constants.TABLENAME_NEW_ORDER: [],
    constants.TABLENAME_ORDER_LINE: [],
    constants.TABLENAME_HISTORY: [],
}
TABLE_FLAGS = {
    constants.TABLENAME_ITEM: [],
    constants.TABLENAME_WAREHOUSE: [],
    constants.TABLENAME_DISTRICT: [],
    constants.TABLENAME_CUSTOMER: [],
    constants.TABLENAME_STOCK: [],
    constants.TABLENAME_ORDERS: [
        ORDER_IS_NEW,
    ],
    constants.TABLENAME_NEW_ORDER: [],
    constants.TABLENAME_ORDER_LINE: [],
    constants.TABLENAME_HISTORY: [],
}


# ==============================================
# RiakDriver
# ==============================================
class RiakDriver(AbstractDriver):

    DEFAULT_CONFIG = {
        "protocol": ("The protocol to riak, http or pbc", "http"),
        "host": ("The hostname to riak", "localhost"),
        "http_port": ("The port number to riak via http", 8098),
        "pbc_port": ("The port number to riak via pbc", 8097),
        "bucket_type": ("The bucket type. Needs to have a correspondingly named search index", "tpcc"),
        "search_index": ("The search index. Needs to have a correspondingly named bucket type", "tpcc"),
        "enable_recording": ("Record all operations", "False"),
        "connect_to_same_node": ("Single node", "False"),

        "ignore_display_code": ("Ignore display code", "True"),
        "use_counters": ("Use counter CRDTs", "True"),
        "sync_next_order_id": ("Synchronize next order id atomic increment with zookeeper", "True"),
        "denormalize": ("Denormalize data", "True"),
        "partition_delivery": ("Partition delivery by warehouse", "True"),
        "use_quantity_crdt": ("Use quantity CRDT", "True"),

        "locking": ("Lock all transaction", "True"),

        "recording_session": ("Name of the session during recording", "riak"),

        "num_servers": ("Total number of servers, for partitioning", "3"),
        "my_server":   ("My server, must be between 1 and num_servers, for partitioning", "3")

    }

    def __init__(self, ddl):
        super(RiakDriver, self).__init__("riak", ddl)
        self.conn = None
        self.bucketType = None
        self.searchIndex = None
        self.transactionCounter = None
        self.curTransaction = ""
        self.isRecording = False
        self.syncNextOrderID = False
        self.operationCounter = None
        self.zk = None
        self.connectToSameNode = None
        self.denormalize = None
        self.use_quantity_crdt = None
        self.partition_delivery = None
        self.ignore_display_code = None
        self.use_counters = None
        self.locking = None
        self.recording_session = None
        self.num_servers = None
        self.my_server = None

    # ----------------------------------------------
    # makeDefaultConfig
    # ----------------------------------------------
    def makeDefaultConfig(self):
        return RiakDriver.DEFAULT_CONFIG

    def pre_log(self):
        return "Client " + str(self.client_id) + " // " if self.client_id is not None else ""

    # ----------------------------------------------
    # loadConfig
    # ----------------------------------------------
    def loadConfig(self, config):

        logging.debug(self.pre_log() + "Loading config")

        for key in RiakDriver.DEFAULT_CONFIG.keys():
            assert key in config, "Missing parameter '%s' in %s configuration" % (key, self.name)

        self.bucketType = config['bucket_type']
        self.searchIndex = config['search_index']
        self.transactionCounter = 0
        self.curTransaction = ""
        self.isRecording = config['enable_recording'] == "True"
        self.operationCounter = 0
        self.syncNextOrderID = config['sync_next_order_id'] == "True"
        self.ignore_display_code = config['ignore_display_code'] == "True"
        self.use_counters = config['use_counters'] == "True"
        self.connectToSameNode = config['connect_to_same_node'] == "True"
        self.denormalize = config['denormalize'] == "True"
        self.use_quantity_crdt = config['use_quantity_crdt'] == "True"
        self.partition_delivery = config['partition_delivery'] == "True"
        self.locking = config['locking'] == "True"
        self.recording_session = config['recording_session']

        self.num_servers = int(config['num_servers'])
        self.my_server = int(config['my_server'])

        #print("isRecording: " + repr(self.isRecording))
        #print("connectToSameNode: " + repr(self.connectToSameNode))
        #print("ignore_display_code: " + repr(self.ignore_display_code))
        #print("use_counters: " + repr(self.use_counters))
        #print("denormalize: " + repr(self.denormalize))
        #print("use_quantity_crdt: " + repr(self.use_quantity_crdt))
        #print("partition_delivery: " + repr(self.partition_delivery))
        #print("syncNextOrderID: " + repr(self.syncNextOrderID))

        if self.connectToSameNode:
            http_port = int(config['http_port'])
            pbc_port = int(config['pbc_port'])
        else:
            http_port = int(config['http_port']) + (self.client_id % 3)
            pbc_port = int(config['pbc_port']) + (self.client_id % 3)

        self.conn = RiakClient(protocol=config['protocol'], host=config['host'],
                               http_port=http_port, pb_port=pbc_port)

        if self.syncNextOrderID or self.locking:
            print("Starting ZooKeeper...")
            self.zk = KazooClient(config['host']+":2181")
            self.zk.start()

        # EARLY BREAK
        test = self.bucket("Test").get("Test", include_context=True)
        test_reg = test.registers["Test"]
        test_reg.assign("Test")
        while True:
            try:
                test.store()
                break
            except RiakError as e:
                time.sleep(1)
                print("retrying operation, since it failed with " + repr(e.value))

        if config["reset"]:
            logging.debug(self.pre_log() + "Deleting all relevant buckets")
            for name in constants.ALL_TABLES:
                for key in self.bucket(name).get_keys():
                    self.bucket(name).delete(key)
                logging.debug(self.pre_log() + "Dropped bucket %s" % name)
                # IF

    # ----------------------------------------------
    # Runtime analysis
    # ----------------------------------------------

    class Dummy(object):

        def __init__(self):
            pass

        def __enter__(self):
            return self

        def __exit__(self, exc_type, exc_val, exc_tb):
            return False

    def transaction(self, name, args):
        self.curTransaction = name
        self.transactionCounter += 1
        if self.locking:
            lock = self.zk.Lock("/"+("/".join(map(lambda x: str(x), args))), self.client_id)
            logging.debug("Client "+str(self.client_id)+" taking lock "+lock.path)
            return lock
        else:
            return self.Dummy()

    def record(self, operation_name, tablename, key_or_query, argument, observed_maps, is_update, program_location):

        self.operationCounter += 1

        # Get original value of map
        original_val = []
        c = 0
        for m in observed_maps:
            if not m.flags["is_deleted"].value:
                original_val.append({})
                for a in TABLE_INDEXES[tablename]:
                    if self.use_counters and (a in TABLE_COUNTERS[tablename]):
                        original_val[c][a] = m.counters[a].value
                    elif a in TABLE_FLAGS[tablename]:
                        original_val[c][a] = repr(m.flags[a].value)
                    elif a in TABLE_SETS[tablename]:
                        original_val[c][a] = json.dumps(m.sets[a].value)
                    else:
                        original_val[c][a] = m.registers[a].value
                c += 1

        client_id = "client" + str(self.client_id)

        # Generate a random operation identifier
        import random
        import string
        op_id = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(16))

        # Acquire the "tracker" set for this client. This reads all newly arrived update operations.
        # Remove all observed values from the set.
        observed_set = set()
        for m in observed_maps:
            # tracker = m.sets["__observed_" + client_id]
            # observed_set |= tracker.value
            # for x in observed_set:
            #    tracker.discard(x)

            # If this is an update operation, generate a random identifier for the update and
            # add them to the "to be observed" set of other clients
            # for i in range(0, self.num_clients):
            #     if i != self.client_id:
            #         other_client_id = "client" + str(i)
            #         tracker = m.sets["__observed_" + other_client_id]
            #        tracker.add(op_id)

            tracker = m.sets["observed"]
            observed_set |= tracker.value
            if is_update:
                tracker.add(op_id)
                if m.modified:
                    m.store()

        # Send the recorded information off to the recorder server
        program_id = self.recording_session
        session_id = self.session_id
        record = {
            "time": long(round(time.time() * 1000000)),
            "cid": client_id,
            "oc": self.operationCounter,
            "pc": program_location,
            "op": {
                "op": operation_name,
                "tablename": tablename,
                "key_or_query": key_or_query,
                "argument": argument,
                "original_val": original_val
            },
            "op_id": op_id,
            "observed_set": dict.fromkeys(observed_set, True),
            "transaction": self.transactionCounter,
            "transaction_name": self.curTransaction
        }

        logging.debug(self.pre_log() + repr(record))
        req = urllib2.Request('http://localhost:5252/record?pid=' + program_id + '&sid=' + session_id)
        req.add_header('Content-Type', 'application/json')
        try:
            urllib2.urlopen(req, json.dumps(record))
        except urllib2.HTTPError as e:
            #logging.error('HTTPError = ' + str(e))
            pass
        except urllib2.URLError as e:
            #logging.error('URLError = ' + str(e))
            pass
        except httplib.HTTPException as e:
            #logging.error('HTTPException = ' + str(e))
            pass

    # ----------------------------------------------
    # Strongly consistent atomic counter
    # ----------------------------------------------
    def zk_inc_get_counter(self, tablename, key, field):
        str_key = self.make_keys(tablename, key)
        counter = RetCounter(self.zk, "/" + tablename + "/" + str_key + "/" + field)
        counter += 1
        return counter.last_value

    # ----------------------------------------------
    # utilities to make me behave more like Mongo
    # ----------------------------------------------

    @staticmethod
    def make_keys(tablename, content):
        index_list = TABLE_INDEXES[tablename]
        key_list = map(lambda x: str(content[x]), index_list)
        return ",".join(key_list)

    def bucket(self, tablename):
        return self.conn.bucket(tablename, self.bucketType)

    def find_one(self, tablename, search_fields, return_fields, display_return_fields, program_location):
        result = self.find(tablename, search_fields, return_fields, display_return_fields, program_location)
        if len(result) >= 1:
            return result[0]
        return None

    def find_by_index(self, tablename, search_fields, comb_return_fields, program_location, primary_index):
        result = []
        search_list = [str(search_fields[x]) for x in primary_index]
        search_request = ",".join(search_list)
        res = self.bucket(tablename).get(search_request)
        result.append(res)
        if self.isRecording:
            self.record("riak_get", tablename, res.key, comb_return_fields, [res], False,
                        program_location)
        return result

    def find_by_search(self, tablename, search_fields, comb_return_fields, program_location):
        result = []
        search_params = []
        for k,v in search_fields.items():
            if self.use_counters and k in TABLE_COUNTERS[tablename]:
                search_params.append(k+"_counter:"+self.make_val_string(v))
            elif k in TABLE_FLAGS[tablename]:
                search_params.append(k+"_flag:"+self.make_val_string(v))
            elif k in TABLE_SETS[tablename]:
                search_params.append(k+"_set:"+self.make_val_string(v))
            else:
                search_params.append(k+"_register:"+self.make_val_string(v))
        search_request = " AND ".join(search_params)
        search_result = self.bucket(tablename).search(search_request, self.searchIndex)
        keys = []
        for doc in search_result["docs"]:
            keys.append(doc["_yz_rk"])
        maps_for_observation = []
        for k in keys:
            res = self.bucket(tablename).get(str(k))
            maps_for_observation.append(res)
            if not res.flags['is_deleted'].value:
                result.append(res)
        if self.isRecording:
            self.record("riak_search", tablename, search_request, comb_return_fields,
                        maps_for_observation, False, program_location)
        return result

    # Returns a list of maps
    def find(self, tablename, search_fields, return_fields, display_return_fields, program_location):
        comb_return_fields = []
        for a in return_fields:
            comb_return_fields.append((a, False))
        for b in display_return_fields:
            comb_return_fields.append((b, self.ignore_display_code))
        primary_index = TABLE_INDEXES[tablename]
        requested_index = search_fields.keys()
        is_simple = all(
                (isinstance(x, str) or isinstance(x, int) or isinstance(x, float)) for x in search_fields.values())
        if is_simple and set(requested_index) == set(primary_index):
            result = self.find_by_index(tablename, search_fields, comb_return_fields, program_location, primary_index)
        else:
            result = self.find_by_search(tablename, search_fields, comb_return_fields, program_location)

        return result

    @staticmethod
    def make_val_string(x):
        if isinstance(x, dict):  # Range
            assert x["from"] is not None
            assert x["to"] is not None
            return "[" + str(x["from"]) + " TO " + str(x["to"]) + "]"
        elif isinstance(x, str):
            return "\"" + x + "\""
        else:
            return str(x)

    def insert(self, tablename, content, program_location):
        key = self.make_keys(tablename, content.get_register_content())
        if key != "":
            res = self.bucket(tablename).new(key)
        else:
            res = self.bucket(tablename).new(None)
        self.update_single(tablename, res, content, program_location)

    def update(self, tablename, query, updates, multi, program_location):
        if multi:
            result = self.find(tablename, query, [], [], program_location)
        else:
            result = [self.find_one(tablename, query, [], [], program_location)]
        assert result is not None
        for r in result:
            self.update_single(tablename, r, updates, program_location)

    def update_single(self, tablename, the_map, updates, program_location):
        for k, v in updates.update.items():
            if k == updates.OP_COUNTER_INC:
                for k2, v2 in v.items():
                    assert (k2 in TABLE_COUNTERS[tablename])
                    if self.syncNextOrderID and k2 == "D_NEXT_O_ID":
                        counter = self.zk.Counter("/" + tablename + "/" + the_map.key + "/" + k2)
                        counter += int(v2)
                    else:
                        the_map.counters[k2].increment(int(v2))
            elif k == updates.OP_FLAG_UPDATE:
                for k2, v2 in v.items():
                    assert (k2 in TABLE_FLAGS[tablename])
                    if v2:
                        the_map.flags[k2].enable()
                    else:
                        the_map.flags[k2].disable()
            elif k == updates.OP_SET_ADD:
                for k2, v2 in v.items():
                    assert (k2 in TABLE_SETS[tablename])
                    for v3 in v2:
                        the_map.sets[k2].add(v3)
            elif k == updates.OP_SET_DISCARD:
                for k2, v2 in v.items():
                    assert (k2 in TABLE_SETS[tablename])
                    for v3 in v2:
                        the_map.sets[k2].discard(v3)
            elif k == updates.OP_REGISTER_ASSIGN:
                for k2, v2 in v.items():
                    assert not ((self.use_counters and k2 in TABLE_COUNTERS[tablename]) or
                                k2 in TABLE_FLAGS[tablename] or
                                k2 in TABLE_SETS[tablename])
                    the_map.registers[k2].assign(str(v2))
        if the_map.modified:
            the_map.store()
        if self.isRecording:
            self.record("riak_update", tablename, the_map.key, updates.update, [the_map], True, program_location)

    def remove(self, tablename, query, program_location):
        for res in self.find(tablename, query, [], [], program_location):
            if self.isRecording:
                res.flags['is_deleted'].enable()
                self.record("riak_delete", tablename, res.key, None, [res], True, program_location)
            else:
                res.delete()

    # ----------------------------------------------
    # loadTuples
    # ----------------------------------------------
    def loadTuples(self, tablename, tuples):
        if len(tuples) == 0:
            return
        logging.debug(self.pre_log() + "Loading %d tuples for tableName %s" % (len(tuples), tablename))

        was_recording = self.isRecording
        self.isRecording = False

        assert tablename in TABLE_COLUMNS, "Unexpected table %s" % tablename
        assert tablename in TABLE_INDEXES, "Unexpected table %s" % tablename
        columns = TABLE_COLUMNS[tablename]
        num_columns = range(len(columns))

        for t in tuples:

            new_tuple = list()
            for i in range(len(t)):
                if isinstance(t[i], datetime):
                    new_tuple.append(int(t[i].strftime("%s")))
                else:
                    new_tuple.append(t[i])

            content = dict(map(lambda x: (columns[x], new_tuple[x]), num_columns))

            if self.denormalize and tablename == constants.TABLENAME_NEW_ORDER:

                query = {}
                for orig, target in DENORMALIZED_TABLE[tablename].items():
                    query[target] = content[orig]

                success = False
                while not success:
                    try:
                        success = True
                        self.update(constants.TABLENAME_ORDERS,
                                    query,
                                    RiakMapUpdate().flag_update(ORDER_IS_NEW, True),
                                    False,
                                    "load_denormalized_new_order")
                    except AttributeError:
                        success = False
                        logging.info("order seems to not exist yet, retrying")
                        time.sleep(1)

            elif self.denormalize and tablename == constants.TABLENAME_ORDER_LINE:

                query = {}
                for orig, target in DENORMALIZED_TABLE[tablename].items():
                    query[target] = content[orig]

                # FIX FIX FIX --- for RMW anomaly
                success = False
                while not success:
                    try:
                        success = True
                        self.update(constants.TABLENAME_ORDERS,
                                    query,
                                    RiakMapUpdate().set_add(ORDER_LINE_SET, [json.dumps(content)]),
                                    False,
                                    "load_denormalized_order_line")
                    except AttributeError:
                        success = False
                        logging.info("order seems to not exist yet, retrying")
                        time.sleep(1)

            elif tablename == constants.TABLENAME_STOCK:

                update = RiakMapUpdate()

                if self.use_counters:
                    for c in TABLE_COUNTERS[constants.TABLENAME_STOCK]:
                        if c is not S_QUANTITY_COUNTER:
                            initial_val = content[c]
                            del content[c]
                            if c is "S_QUANTITY" and self.use_quantity_crdt:
                                update.counter_inc(S_QUANTITY_COUNTER, initial_val - 10)
                            else:
                                update.counter_inc(c, initial_val)

                update.registers_assign(content)

                self.insert(tablename, update, "load_stock")

            else:

                update = RiakMapUpdate()

                if self.use_counters:
                    for c in TABLE_COUNTERS[tablename]:
                        initial_val = content[c]
                        del content[c]
                        update.counter_inc(c, initial_val)

                update.registers_assign(content)

                self.insert(tablename, RiakMapUpdate().registers_assign(content), "load_registers")

        self.isRecording = was_recording

        return

    # ----------------------------------------------
    # loadFinishDistrict
    # ----------------------------------------------
    def loadFinishDistrict(self, w_id, d_id):
        logging.info("Finished loading district")

    # ----------------------------------------------
    # loadFinish
    # ----------------------------------------------
    def loadFinish(self):
        logging.info("Finished loading tables")
        if logging.getLogger().isEnabledFor(logging.DEBUG):
            for name in constants.ALL_TABLES:
                logging.debug(self.pre_log() + "%-12s%d records" % (name + ":", len(self.bucket(name).get_keys())))

    # ----------------------------------------------
    # doDelivery
    # ----------------------------------------------
    def doDelivery(self, params):

        w_id = params["w_id"]
        o_carrier_id = params["o_carrier_id"]
        ol_delivery_d = int(params["ol_delivery_d"].strftime("%s"))
        result = []

        # PARTITION PROCESSING OF DELIVERIES!!!
        # TODO: Slightly messes up transaction count
        if self.partition_delivery and w_id % self.num_servers != self.my_server:
            print("Skipping delivery for warehouse I do not own")
            return result

        for d_id in range(1, constants.DISTRICTS_PER_WAREHOUSE + 1):

            with self.transaction("delivery", [w_id, d_id]):
                if self.locking:
                    logging.debug("Client "+str(self.client_id)+" got the lock!")

                # getNewOrder
                if self.denormalize:

                    o = self.find_one(constants.TABLENAME_ORDERS,
                                      {"O_D_ID": d_id, "O_W_ID": w_id, ORDER_IS_NEW: "True"},
                                      ["O_C_ID", "O_ID", ORDER_LINE_SET], [], "delivery_2")

                    if o is None or o.registers["O_C_ID"].value == "" or o.registers["O_ID"].value == "":
                        # No new orders for this district
                        continue

                    o_id = int(o.registers["O_ID"].value)
                    c_id = int(o.registers["O_C_ID"].value)

                    ol_total = 0
                    for ol in o.sets[ORDER_LINE_SET]:
                        ol_struct = json.loads(ol)
                        ol_total += ol_struct["OL_AMOUNT"]

                else:
                    no = self.find_one(constants.TABLENAME_NEW_ORDER, {"NO_D_ID": d_id, "NO_W_ID": w_id},
                                       ["NO_O_ID"], [], "delivery_1")

                    if no is None or no.registers["NO_O_ID"].value == "":
                        # No orders for this district: skip it. Note: This must be reported if > 1%
                        continue

                    o_id = int(no.registers["NO_O_ID"].value)

                    # getCId
                    o = self.find_one(constants.TABLENAME_ORDERS,
                                      {"O_ID": o_id, "O_D_ID": d_id, "O_W_ID": w_id},
                                      ["O_C_ID"], [], "delivery_2")
                    if o is None or o.registers["O_ID"].value is "":
                        # We see only part of the order. Skip.
                        # This cannot happen under serializable transactions.
                        logging.info("Seems like I partially observed a transaction? Missing synchronization." +
                                     "doDelivery on " + repr({"O_ID": o_id, "O_D_ID": d_id, "O_W_ID": w_id}))
                        continue

                    c_id = int(o.registers["O_C_ID"].value)

                    # sumOLAmount
                    order_lines = self.find(constants.TABLENAME_ORDER_LINE,
                                            {"OL_O_ID": o_id, "OL_D_ID": d_id, "OL_W_ID": w_id},
                                            ["OL_AMOUNT"], [], "delivery_3")
                    assert order_lines is not None
                    ol_total = sum([float(ol.registers["OL_AMOUNT"].value) for ol in order_lines])

                    # If there are no order lines, SUM returns null. There should always be order lines.
                    if (ol_total is None) or (ol_total == 0.0):
                        # We see only part of the order. Skip.
                        # This cannot happen under serializable transactions.
                        logging.info("Seems like I partially observed a transaction? Missing synchronization." +
                                     "doDelivery on " + repr({"O_ID": o_id, "O_D_ID": d_id, "O_W_ID": w_id}))
                        continue
                
                # updateOrders
                update = RiakMapUpdate()
                update.registers_assign({
                    "O_CARRIER_ID": o_carrier_id
                })

                # updateOrderLine, deleteNewOrder
                if self.denormalize:
                    to_add = []
                    to_discard = []
                    for a in o.sets[ORDER_LINE_SET]:
                        a_struct = json.loads(a)
                        a_struct["OL_DELIVERY_D"] = ol_delivery_d
                        a_new = json.dumps(a_struct)
                        to_add.append(a_new)
                        to_discard.append(a)
                    update.set_discard(ORDER_LINE_SET, to_discard)
                    update.set_add(ORDER_LINE_SET, to_add)
                    update.flag_update(ORDER_IS_NEW, False)
                else:
                    self.update(constants.TABLENAME_ORDER_LINE,
                                {"OL_O_ID": o_id, "OL_D_ID": d_id, "OL_W_ID": w_id},
                                RiakMapUpdate().registers_assign({"OL_DELIVERY_D": ol_delivery_d}), True, "delivery_5")
                    self.remove(constants.TABLENAME_NEW_ORDER, {"NO_O_ID": o_id, "NO_D_ID": d_id, "NO_W_ID": w_id},
                                "delivery_7")

                self.update_single(constants.TABLENAME_ORDERS, o, update, "delivery_4")

                # updateCustomer
                if self.use_counters:
                    self.update(constants.TABLENAME_CUSTOMER, {"C_ID": c_id, "C_D_ID": d_id, "C_W_ID": w_id},
                                RiakMapUpdate().counter_inc("C_BALANCE", ol_total), False, "delivery_6")
                else:
                    c = self.find_one(constants.TABLENAME_CUSTOMER, {"C_ID": c_id, "C_D_ID": d_id, "C_W_ID": w_id},
                                      ["C_BALANCE"], [], "delivery_6")
                    new_bal = float(c.registers["C_BALANCE"].value) + ol_total
                    self.update(constants.TABLENAME_CUSTOMER, {"C_ID": c_id, "C_D_ID": d_id, "C_W_ID": w_id},
                                RiakMapUpdate().register_assign("C_BALANCE", new_bal), False, "delivery_6")

                # These must be logged in the "result file" according to TPC-C 2.7.2.2 (page 39)
                # We remove the queued time, completed time, w_id, and o_carrier_id: the client can figure
                # them out

                result.append((d_id, o_id))

        # FOR
        return result

    # ----------------------------------------------
    # doNewOrder
    # ----------------------------------------------
    def doNewOrder(self, params):

        w_id = params["w_id"]
        d_id = params["d_id"]
        c_id = params["c_id"]
        o_entry_d = int(params["o_entry_d"].strftime("%s"))
        i_ids = params["i_ids"]
        i_w_ids = params["i_w_ids"]
        i_qtys = params["i_qtys"]
        s_dist_col = "S_DIST_%02d" % d_id + ""

        with self.transaction("new_order", [w_id, d_id]):
            if self.locking:
                logging.debug("Client "+str(self.client_id)+" got the lock!")

            assert len(i_ids) > 0
            assert len(i_ids) == len(i_w_ids)
            assert len(i_ids) == len(i_qtys)

            # http://stackoverflow.com/q/3844931/
            all_local = (not i_w_ids or [w_id] * len(i_w_ids) == i_w_ids)

            items = []
            for i_id in i_ids:
                items.append(self.find_one(constants.TABLENAME_ITEM, {"I_ID": i_id},
                                           ["I_ID", "I_PRICE", "I_NAME", "I_DATA"], [], "new_order_1"))

            # TPCC defines 1% of neworder gives a wrong itemid, causing rollback.
            # Note that this will happen with 1% of transactions on purpose.
            if len(items) != len(i_ids):
                print("Broken new order transaction!")
                sys.exit(1)
            # IF

            # ----------------
            # Collect Information from WAREHOUSE, DISTRICT, and CUSTOMER
            # ----------------

            # getWarehouseTaxRate
            w = self.find_one(constants.TABLENAME_WAREHOUSE, {"W_ID": w_id}, ["W_TAX"], [], "new_order_2")
            assert w
            w_tax = float(w.registers["W_TAX"].value)

            # getDistrict
            if self.syncNextOrderID:
                d = self.find_one(constants.TABLENAME_DISTRICT, {"D_ID": d_id, "D_W_ID": w_id},
                                  ["D_TAX"], [], "new_order_3")
                d_next_o_id = self.zk_inc_get_counter(constants.TABLENAME_DISTRICT,
                                                      {"D_ID": d_id, "D_W_ID": w_id}, "D_NEXT_O_ID")
                d_tax = float(d.registers["D_TAX"].value)
            elif self.use_counters:
                d = self.find_one(constants.TABLENAME_DISTRICT, {"D_ID": d_id, "D_W_ID": w_id},
                                  ["D_TAX", "D_NEXT_O_ID"], [], "new_order_4")
                d_next_o_id = int(d.counters["D_NEXT_O_ID"].value)
                d_tax = float(d.registers["D_TAX"].value)
                self.update_single(constants.TABLENAME_DISTRICT, d,
                                   RiakMapUpdate().counter_inc("D_NEXT_O_ID", 1), "new_order_5")
            else:
                d = self.find_one(constants.TABLENAME_DISTRICT, {"D_ID": d_id, "D_W_ID": w_id},
                                  ["D_TAX", "D_NEXT_O_ID"], [], "new_order_4")
                d_next_o_id = int(d.registers["D_NEXT_O_ID"].value)
                d_tax = float(d.registers["D_TAX"].value)
                self.update_single(constants.TABLENAME_DISTRICT, d,
                                   RiakMapUpdate().register_assign("D_NEXT_O_ID", d_next_o_id + 1), "new_order_5")

            # getCustomer
            c = self.find_one(constants.TABLENAME_CUSTOMER, {"C_ID": c_id, "C_D_ID": d_id, "C_W_ID": w_id},
                              ["C_DISCOUNT", "C_LAST", "C_CREDIT"], [], "new_order_6")
            assert c
            assert c.registers["C_DISCOUNT"].value is not ""
            c_discount = float(c.registers["C_DISCOUNT"].value)

            # ----------------
            # Insert Order Information
            # ----------------
            ol_cnt = len(i_ids)
            o_carrier_id = constants.NULL_CARRIER_ID

            o = RiakMapUpdate().registers_assign(
                    {"O_ID": d_next_o_id, "O_ENTRY_D": o_entry_d, "O_CARRIER_ID": o_carrier_id,
                     "O_OL_CNT": ol_cnt, "O_ALL_LOCAL": all_local, "O_D_ID": d_id, "O_W_ID": w_id,
                     "O_C_ID": c_id})

            # createNewOrder
            if self.denormalize:
                o.flag_update(ORDER_IS_NEW, True)
            else:
                self.insert(constants.TABLENAME_NEW_ORDER,
                            RiakMapUpdate().registers_assign({"NO_O_ID": d_next_o_id, "NO_D_ID": d_id, "NO_W_ID": w_id}),
                            "new_order_7")

            # ----------------
            # Insert Order Item Information
            # ----------------
            item_data = []
            total = 0
            for i in range(ol_cnt):
                ol_number = i + 1
                ol_supply_w_id = i_w_ids[i]
                ol_i_id = i_ids[i]
                ol_quantity = i_qtys[i]

                item_info = items[i]
                i_name = item_info.registers["I_NAME"].value
                i_data = item_info.registers["I_DATA"].value
                i_price = float(item_info.registers["I_PRICE"].value)

                if self.use_quantity_crdt:
                    stock_read = ["S_I_ID", s_dist_col]
                    stock_read_display = ["S_DATA", S_QUANTITY_COUNTER]
                elif self.use_counters:
                    stock_read = ["S_I_ID", "S_DATA", s_dist_col, "S_QUANTITY"]
                    stock_read_display = ["S_DATA", S_QUANTITY_COUNTER]
                else:
                    stock_read = ["S_I_ID", "S_DATA", s_dist_col, "S_QUANTITY", "S_YTD", "S_ORDER_CNT", "S_REMOTE_CNT"]
                    stock_read_display = ["S_DATA", S_QUANTITY_COUNTER]

                # getStockInfo
                si = self.find_one(constants.TABLENAME_STOCK, {"S_I_ID": ol_i_id, "S_W_ID": w_id},
                                   stock_read, stock_read_display, "new_order_10")
                assert si, "Failed to find S_I_ID: %d" % ol_i_id

                s_data = si.registers["S_DATA"].value
                s_dist_xx = si.registers[s_dist_col].value  # Fetches data from the s_dist_[d_id] column

                stock_update = RiakMapUpdate()

                # Update stock
                if self.use_counters:
                    stock_update.counter_inc("S_YTD", ol_quantity)
                    stock_update.counter_inc("S_ORDER_CNT", 1)

                    if self.use_quantity_crdt:
                        s_quantity = 10 + (-int(si.counters[S_QUANTITY_COUNTER].value) % 91)
                        stock_update.counter_inc(S_QUANTITY_COUNTER, ol_quantity)
                    else:
                        s_quantity = int(si.counters["S_QUANTITY"].value)
                        if s_quantity >= ol_quantity + 10:
                            stock_update.counter_inc("S_QUANTITY", -ol_quantity)
                        else:
                            stock_update.counter_inc("S_QUANTITY", -ol_quantity + 91)

                    if ol_supply_w_id != w_id:
                        stock_update.counter_inc("S_REMOTE_CNT", 1)
                else:
                    stock_update.register_assign("S_YTD", int(si.registers["S_YTD"].value) + ol_quantity)
                    stock_update.register_assign("S_ORDER_CNT", int(si.registers["S_ORDER_CNT"].value) + 1)

                    s_quantity = int(si.registers["S_QUANTITY"].value)
                    if s_quantity >= ol_quantity + 10:
                        stock_update.register_assign("S_QUANTITY", s_quantity-ol_quantity)
                    else:
                        stock_update.register_assign("S_QUANTITY", s_quantity-ol_quantity + 91)

                    if ol_supply_w_id != w_id:
                        stock_update.register_assign("S_REMOTE_CNT", int(si.registers["S_REMOTE_CNT"].value) + 1)

                # updateStock
                self.update_single(constants.TABLENAME_STOCK, si, stock_update, "new_order_11")

                if i_data.find(constants.ORIGINAL_STRING) != -1 and s_data.find(constants.ORIGINAL_STRING) != -1:
                    brand_generic = 'B'
                else:
                    brand_generic = 'G'
                # Transaction profile states to use "ol_quantity * i_price"
                ol_amount = ol_quantity * i_price
                total += ol_amount

                ol = RiakMapUpdate().registers_assign({"OL_O_ID": d_next_o_id,
                                                       "OL_NUMBER": ol_number,
                                                       "OL_I_ID": ol_i_id,
                                                       "OL_SUPPLY_W_ID": ol_supply_w_id,
                                                       "OL_DELIVERY_D": o_entry_d,
                                                       "OL_QUANTITY": ol_quantity,
                                                       "OL_AMOUNT": ol_amount,
                                                       "OL_DIST_INFO": s_dist_xx,
                                                       "OL_D_ID": d_id,
                                                       "OL_W_ID": w_id})

                # createOrderLine
                if self.denormalize:
                    o.set_add(ORDER_LINE_SET, [json.dumps(ol.get_register_content())])
                else:
                    self.insert(constants.TABLENAME_ORDER_LINE, ol, "new_order_12")

                # Add the info to be returned
                item_data.append((i_name, brand_generic, i_price, ol_amount, s_quantity))
            # FOR

            # createOrder
            self.insert(constants.TABLENAME_ORDERS, o, "new_order_8")

            # Adjust the total for the discount
            # print "c_discount:", c_discount, type(c_discount)
            # print "w_tax:", w_tax, type(w_tax)
            # print "d_tax:", d_tax, type(d_tax)
            total *= (1 - c_discount) * (1 + w_tax + d_tax)

            # Pack up values the client is missing (see TPC-C 2.4.3.5)
            misc = [(w_tax, d_tax, d_next_o_id, total)]

        return [c, misc, item_data]

    # ----------------------------------------------
    # doOrderStatus
    # ----------------------------------------------
    def doOrderStatus(self, params):

        w_id = params["w_id"]
        d_id = params["d_id"]
        c_id = params["c_id"]
        c_last = params["c_last"]

        with self.transaction("order_status", [w_id, d_id]):
            if self.locking:
                logging.debug("Client "+str(self.client_id)+" got the lock!")

            assert w_id, pformat(params)
            assert d_id, pformat(params)

            search_fields = {"C_W_ID": w_id, "C_D_ID": d_id}

            if c_id is not None:
                # getCustomerByCustomerId
                search_fields["C_ID"] = c_id
                c = self.find_one(constants.TABLENAME_CUSTOMER, search_fields, [],
                                  TABLE_COLUMNS.get(constants.TABLENAME_CUSTOMER), "order_status_1")
                assert c

            else:
                # getCustomersByLastName
                # Get the midpoint customer's id
                search_fields['C_LAST'] = c_last

                all_customers = self.find(constants.TABLENAME_CUSTOMER, search_fields, [],
                                          TABLE_COLUMNS.get(constants.TABLENAME_CUSTOMER), "order_status_2")
                name_count = len(all_customers)
                assert name_count > 0
                index = (name_count - 1) / 2
                c = all_customers[index]
                c_id = int(c.registers["C_ID"].value)
            assert c is not None
            assert c_id is not None

            # getLastOrder
            order_lines = []
            if self.denormalize:
                orders = self.find(constants.TABLENAME_ORDERS, {"O_W_ID": w_id, "O_D_ID": d_id, "O_C_ID": c_id},
                                   [], ["O_ID", "O_CARRIER_ID", "O_ENTRY_D", ORDER_LINE_SET], "order_status_3")
                first_order = sorted(orders, key=lambda x: int(x.registers["O_ID"].value), reverse=True)[0]
                if first_order:
                    order_lines = map(lambda x: json.loads(x), first_order.sets[ORDER_LINE_SET])
            else:
                orders = self.find(constants.TABLENAME_ORDERS, {"O_W_ID": w_id, "O_D_ID": d_id, "O_C_ID": c_id},
                                   [], ["O_ID", "O_CARRIER_ID", "O_ENTRY_D"], "order_status_3")
                first_order = sorted(orders, key=lambda x: int(x.registers["O_ID"].value), reverse=True)[0]

                if first_order:
                    o_id = int(first_order.registers["O_ID"].value)
                    order_lines = self.find(constants.TABLENAME_ORDER_LINE,
                                            {"OL_O_ID": o_id, "OL_D_ID": d_id, "OL_W_ID": w_id}, [],
                                            ["OL_SUPPLY_W_ID", "OL_I_ID", "OL_QUANTITY", "OL_AMOUNT", "OL_DELIVERY_D"],
                                            "order_status_4")

        return [c, first_order, order_lines]

    # ----------------------------------------------
    # doPayment
    # ----------------------------------------------    
    def doPayment(self, params):

        w_id = params["w_id"]
        d_id = params["d_id"]
        h_amount = params["h_amount"]
        c_w_id = params["c_w_id"]
        c_d_id = params["c_d_id"]
        c_id = params["c_id"]
        c_last = params["c_last"]
        h_date = int(params["h_date"].strftime("%s"))

        with self.transaction("payment", [w_id, d_id]):
            if self.locking:
                logging.debug("Client "+str(self.client_id)+" got the lock!")

            if self.use_counters:
                read_for_computation = [
                    "C_ID",  # INTEGER
                    "C_CREDIT",  # VARCHAR
                    "C_DATA",  # VARCHAR
                ]
            else:
                read_for_computation = [
                    "C_ID",  # INTEGER
                    "C_CREDIT",  # VARCHAR
                    "C_DATA",  # VARCHAR
                    "C_BALANCE",  # FLOAT
                    "C_YTD_PAYMENT",  # FLOAT
                    "C_PAYMENT_CNT",  # INTEGER
                ]

            read_for_display = [x for x in TABLE_COLUMNS[constants.TABLENAME_CUSTOMER] if x not in read_for_computation]

            # ====== GET CUSTOMER
            if c_id is not None:
                # getCustomerByCustomerId
                c = self.find_one(constants.TABLENAME_CUSTOMER, {"C_W_ID": w_id, "C_D_ID": d_id, "C_ID": c_id},
                                  read_for_computation, read_for_display, "payment_1")
            else:
                # getCustomersByLastName
                # Get the midpoint customer's id
                all_customers = self.find(constants.TABLENAME_CUSTOMER,
                                          {"C_W_ID": w_id, "C_D_ID": d_id, "C_LAST": c_last},
                                          read_for_computation, read_for_display, "payment_2")
                namecnt = len(all_customers)
                assert namecnt > 0
                index = (namecnt - 1) / 2
                c = all_customers[index]
                c_id = int(c.registers["C_ID"].value)
            assert c is not None
            assert c_id is not None
            c_data = c.registers["C_DATA"].value

            # ====== GET WAREHOUSE
            if self.use_counters:
                w = self.find_one(constants.TABLENAME_WAREHOUSE, {"W_ID": w_id}, [],
                                  ["W_NAME", "W_STREET_1", "W_STREET_2", "W_CITY", "W_STATE", "W_ZIP"], "payment_3")
                self.update_single(constants.TABLENAME_WAREHOUSE, w,
                                   RiakMapUpdate().counter_inc("W_YTD", h_amount), "payment_4")
            else:
                w = self.find_one(constants.TABLENAME_WAREHOUSE, {"W_ID": w_id}, ["W_YTD"],
                                  ["W_NAME", "W_STREET_1", "W_STREET_2", "W_CITY", "W_STATE", "W_ZIP"], "payment_3")
                self.update_single(constants.TABLENAME_WAREHOUSE, w,
                                   RiakMapUpdate().register_assign("W_YTD",
                                                                   float(w.registers["W_YTD"].value) + h_amount),
                                   "payment_4")

            # getDistrict
            if self.use_counters:
                d = self.find_one(constants.TABLENAME_DISTRICT, {"D_W_ID": w_id, "D_ID": d_id}, [],
                                  ["D_NAME", "D_STREET_1", "D_STREET_2", "D_CITY", "D_STATE", "D_ZIP"], "payment_5")
                self.update_single(constants.TABLENAME_DISTRICT, d,
                                   RiakMapUpdate().counter_inc("D_YTD", h_amount), "payment_6")
            else:
                d = self.find_one(constants.TABLENAME_DISTRICT, {"D_W_ID": w_id, "D_ID": d_id}, ["D_YTD"],
                                  ["D_NAME", "D_STREET_1", "D_STREET_2", "D_CITY", "D_STATE", "D_ZIP"], "payment_5")
                self.update_single(constants.TABLENAME_DISTRICT, d,
                                   RiakMapUpdate().register_assign("D_YTD",
                                                                   float(d.registers["D_YTD"].value) + h_amount),
                                   "payment_6")

            # == updateCustomer
            if self.use_counters:
                customer_update = RiakMapUpdate() \
                    .counter_inc("C_BALANCE", h_amount * -1) \
                    .counter_inc("C_YTD_PAYMENT", h_amount) \
                    .counter_inc("C_PAYMENT_CNT", 1)
            else:
                customer_update = RiakMapUpdate() \
                    .register_assign("C_BALANCE", float(c.registers["C_BALANCE"].value) + h_amount * -1) \
                    .register_assign("C_YTD_PAYMENT", float(c.registers["C_YTD_PAYMENT"].value) + h_amount) \
                    .register_assign("C_PAYMENT_CNT", float(c.registers["C_PAYMENT_CNT"].value) + 1)

            # Customer Credit Information
            if c.registers["C_CREDIT"].value == constants.BAD_CREDIT:
                new_data = " ".join(map(str, [c_id, c_d_id, c_w_id, d_id, w_id, h_amount]))
                c_data = (str(new_data) + "|" + str(c_data))
                if len(c_data) > constants.MAX_C_DATA:
                    c_data = c_data[:constants.MAX_C_DATA]
                customer_update.register_assign("C_DATA", c_data)

            self.update(constants.TABLENAME_CUSTOMER, {"C_W_ID": w_id, "C_D_ID": d_id, "C_ID": c_id},
                        customer_update, False, "payment_7")

            # == insertHistory
            # Concatenate w_name, four spaces, d_name
            h_data = "%s    %s" % (w.registers["W_NAME"].value, d.registers["D_NAME"].value)
            h = RiakMapUpdate().registers_assign({"H_D_ID": d_id, "H_W_ID": w_id, "H_DATE": h_date, "H_DATA": h_data})
            self.insert(constants.TABLENAME_HISTORY, h, "payment_8")

        # TPC-C 2.5.3.3: Must display the following fields:
        # W_ID, D_ID, C_ID, C_D_ID, C_W_ID, W_STREET_1, W_STREET_2, W_CITY, W_STATE, W_ZIP, 
        # D_STREET_1, D_STREET_2, D_CITY, D_STATE, D_ZIP, C_FIRST, C_MIDDLE, C_LAST, C_STREET_1, 
        # C_STREET_2, C_CITY, C_STATE, C_ZIP, C_PHONE, C_SINCE, C_CREDIT, C_CREDIT_LIM, 
        # C_DISCOUNT, C_BALANCE, the first 200 characters of C_DATA (only if C_CREDIT = "BC"), 
        # H_AMOUNT, and H_DATE.
        # Hand back all the warehouse, district, and customer data
        return [w, d, c]

    # ----------------------------------------------
    # doStockLevel
    # ----------------------------------------------    
    def doStockLevel(self, params):

        w_id = params["w_id"]
        d_id = params["d_id"]
        threshold = params["threshold"]

        with self.transaction("stock_level", [w_id, d_id]):
            if self.locking:
                logging.debug("Client "+str(self.client_id)+" got the lock!")

            # getOId
            if self.syncNextOrderID:
                o_id = self.zk_inc_get_counter(constants.TABLENAME_DISTRICT, {"D_ID": d_id, "D_W_ID": w_id},
                                               "D_NEXT_O_ID")
            elif self.use_counters:
                district = self.find_one(constants.TABLENAME_DISTRICT, {"D_W_ID": w_id, "D_ID": d_id}, ["D_NEXT_O_ID"],
                                         [], "stock_level_1")
                o_id = district.counters["D_NEXT_O_ID"].value
            else:
                district = self.find_one(constants.TABLENAME_DISTRICT, {"D_W_ID": w_id, "D_ID": d_id}, ["D_NEXT_O_ID"],
                                         [], "stock_level_1")
                assert district.registers["D_NEXT_O_ID"].value is not ""
                o_id = int(district.registers["D_NEXT_O_ID"].value)

            # getStockCount
            if self.denormalize:
                orders = self.find(constants.TABLENAME_ORDERS,
                                   {"O_ID": {"from": (o_id - 20), "to": (o_id - 1)},
                                    "D_ID": d_id, "W_ID": w_id}, [],
                                   [ORDER_LINE_SET], "stock_level_2")
                ol_ids = set()
                for a in orders:
                    for b in a.sets(ORDER_LINE_SET):
                        ol = json.loads(b)
                        ol_ids.add(int(ol.registers["OL_I_ID"].value))

            else:

                order_lines = self.find(constants.TABLENAME_ORDER_LINE,
                                        {"OL_O_ID": {"from": (o_id - 20), "to": (o_id - 1)},
                                         "OL_D_ID": d_id, "OL_W_ID": w_id}, [],
                                        ["OL_I_ID"], "stock_level_2")

                ol_ids = set()
                for ol in order_lines:
                    ol_ids.add(int(ol.registers["OL_I_ID"].value))
                    # FOR

            result = []
            for ol_id in ol_ids:
                result.append(self.find(constants.TABLENAME_STOCK,
                                        {"S_W_ID": w_id, "S_I_ID": ol_id,
                                         "S_QUANTITY": {"from": "*", "to": (threshold - 1)}},
                                        [], [], "stock_level_3"))

        return len(result)

# CLASS
