class RiakMapUpdate:
    OP_SET_ADD = "__op_set_add"
    OP_SET_DISCARD = "__op_set_discard"
    OP_FLAG_UPDATE = "__op_flag_update"
    OP_REGISTER_ASSIGN = "__op_register_assign"
    OP_COUNTER_INC = "__op_counter_inc"

    def __init__(self):
        self.update = {}

    def get_register_content(self):
        assert self.OP_REGISTER_ASSIGN in self.update
        return self.update[self.OP_REGISTER_ASSIGN]

    def make_def(self, field, value):
        if field not in self.update:
            self.update[field] = value
        return self

    def flag_update(self, name, value):
        self.make_def(self.OP_FLAG_UPDATE, {})
        self.update[self.OP_FLAG_UPDATE][name] = value
        return self

    def register_assign(self, name, value):
        self.make_def(self.OP_REGISTER_ASSIGN, {})
        self.update[self.OP_REGISTER_ASSIGN][name] = value
        return self

    def registers_assign(self, values):
        self.make_def(self.OP_REGISTER_ASSIGN, {})
        for name, value in values.items():
            self.update[self.OP_REGISTER_ASSIGN][name] = value
        return self

    def counter_inc(self, name, value):
        self.make_def(self.OP_COUNTER_INC, {})
        self.update[self.OP_COUNTER_INC][name] = value
        return self

    def set_add(self, name, value):
        assert isinstance(value, list)
        self.make_def(self.OP_SET_ADD, {})
        self.update[self.OP_SET_ADD][name] = value
        return self

    def set_discard(self, name, value):
        assert isinstance(value, list)
        self.make_def(self.OP_SET_DISCARD, {})
        self.update[self.OP_SET_DISCARD][name] = value
        return self
