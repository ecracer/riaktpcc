from setuptools import setup, find_packages

version = '0.0'

setup(name='py-tpcc',
      version=version,
      description="Python/Riak implementation of the TPC-C benchmark",
      long_description="",
      classifiers=[],
      keywords='',
      author='Lucas Brutschy',
      author_email='lucas.brutschy@inf.ethz.ch',
      url='http://people.inf.ethz.ch/blucas/',
      license='BSD',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
            'riak', 'kazoo'
      ],
      entry_points="""
      """,
      )
